/**
 * Distributed & Mobile Systems: S1 2016 Assignment 4: WebServices and Messaging
 *
 * @author Sez Prouting
 */
package sezmeralda.clockme.dt;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import sezmeralda.clockme.dt.HttpClockAccessor;
import sezmeralda.clockme.dt.MyClockPanel;

public class SharePoller implements Runnable{
    protected final HttpClockAccessor ca = new HttpClockAccessor();
    protected final MyClockPanel panel;
    protected final ClockShareMessageHandler msgHandler;
    protected final int userID;
    protected final String shareEnquiryMessage;
    protected static final String DELIM=",", SUCCESS="1";
    public static final int SHARE_CLOCK=0,
                            SHARE_ENQUIRY=1,
                            ACCEPT_SHARE=2,
                            DECLINE_SHARE=3,
                            FOUND=1,
                            NOT_FOUND=0,
                            DELAY=10000;
    protected Timer timer;
    
    public SharePoller(MyClockPanel clockPanel, ClockShareMessageHandler msgHandler, int userID) {
        this.panel = clockPanel;
        this.msgHandler = msgHandler;
        this.userID = userID;
        shareEnquiryMessage = SHARE_ENQUIRY + ","
                            + userID;
        
        ActionListener timerListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                sendShareEnquiry();
            }
        };
        timer = new Timer(DELAY, timerListener);
    }
    
    @Override
    public void run() {
        sendShareEnquiry();
        timer.start();
    }
    
    private void sendShareEnquiry(){
        String response = msgHandler.sendMessage(shareEnquiryMessage);
        StringTokenizer tokeniser = new StringTokenizer(response, DELIM);
        
        int shareID, clockOwnerID, clockNum;
        int hasShare = Integer.parseInt(tokeniser.nextToken());
        
        if(hasShare == FOUND){
            shareID = Integer.parseInt(tokeniser.nextToken());
            clockOwnerID = Integer.parseInt(tokeniser.nextToken());
            clockNum = Integer.parseInt(tokeniser.nextToken());
            
            String clockOwnerName = ca.MANAGER.getUserName(""+clockOwnerID);
            
            int willShare = JOptionPane.showOptionDialog(null, 
                    (clockOwnerName + " has chosen to share a clock with you." +
                    "\nThe clock to be shared is Clock#" + clockNum +
                    "\n\nWill you ACCEPT this clock share?"), 
                    "New Clock Share Received", 
                    JOptionPane.YES_NO_OPTION, 
                    JOptionPane.INFORMATION_MESSAGE, 
                    null, null, 
                    JOptionPane.YES_OPTION);
            
            if(willShare == JOptionPane.YES_OPTION){
                System.out.println("User has accepted a share");
                sendShareAcceptance(shareID, clockNum, clockOwnerName, clockOwnerID);
            }
            else {
                System.out.println("User declined the share");
                sendShareDecline(shareID);
            }
            
        }
    }
    
    
    public void sendShareAcceptance(final int shareID, final int clockNum,
                                    final String ownerName, final int ownerID){
        System.out.println("share accept thinks owner is: " + ownerName);
        //SEND MESSAGE
        String message = ACCEPT_SHARE + ","  + shareID;
        String acceptanceSuccess = msgHandler.sendMessage(message);
        
        //UPDATE PANEL
        if(acceptanceSuccess.equals(SUCCESS)){
            String date = ca.MANAGER.getClock(""+ownerID, ""+clockNum);
            panel.addSharedClock(ownerName, date);
        }
            
    }
    
    public void sendShareDecline(final int shareID){
        String message = DECLINE_SHARE + ","
                        + shareID;
        msgHandler.sendMessage(message);
    }
}
