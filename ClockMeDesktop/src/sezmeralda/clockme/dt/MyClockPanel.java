/**
 * Distributed & Mobile Systems: S1 2016
 * Assignment 4: WebServices and Messaging
 *
 * @author Sez Prouting
 */
package sezmeralda.clockme.dt;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;
//import javax.json.JsonArray;
//import javax.json.JsonString;
//import javax.json.JsonValue;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class MyClockPanel extends JPanel{
    
    protected final ClockPanel[] myClocks;
    protected final SharePanel sharePanel;
    protected final int userID;
    protected final String userName;
    protected Thread msgThread, subscribeThread;
    protected static final int LAYOUT_SPACER=5, WINDOW_WIDTH=240, WINDOW_HEIGHT=410, USER_NOT_FOUND=-1;
    protected static final String   DATE_FORMAT="hh:mm a dd-MMM-YY",
                                    CLOCK_O = "Clock 1",
                                    CLOCK_1 = "Clock 2",
                                    CLOCK_2 = "Clock 3",
                                    CLOCK_3 = "Clock 4",
                                    CLOCK_4 = "Clock 5";
    protected static final SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT);
    protected static final HttpClockAccessor ca = new HttpClockAccessor();
    protected static final ClockShareMessageHandler msgHandler = new ClockShareMessageHandler();
    protected static final MenuListener menuListener = new MenuListener();
    
    public MyClockPanel(int userID, String uname) {
       // dateFormatter = new SimpleDateFormat(DATE_FORMAT);
        this.userID = userID;
        this.userName = uname;
        msgThread = new Thread(new SharePoller(this, msgHandler, userID));
        subscribeThread = new Thread(new UpdateSubscriber(this));
        myClocks = new ClockPanel[]{
                            new ClockPanel(CLOCK_O, 0), new ClockPanel(CLOCK_1, 1),
                            new ClockPanel(CLOCK_2, 2),new ClockPanel(CLOCK_3, 3),new ClockPanel(CLOCK_4, 4)};
        sharePanel = new SharePanel();
    }
    
    protected void init(){
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        for(ClockPanel cp : myClocks){
            cp.init();
            add(cp);
            add(Box.createVerticalStrut(20));
        }
        add(sharePanel);
        
        String sharedClocks = ca.MANAGER.getAcceptedShares(""+userID);
        System.out.println("Have received the following clocks: " + sharedClocks);
        StringTokenizer tokeniser = new StringTokenizer(sharedClocks, ";");
        StringTokenizer pairTokeniser;
        while(tokeniser.hasMoreTokens()){
            String sharePair = tokeniser.nextToken();
            pairTokeniser = new StringTokenizer(sharePair, ",");
            String ownerName = pairTokeniser.nextToken();
            String clockZone = pairTokeniser.nextToken();
            sharePanel.addClock(ownerName, clockZone);
        }
    }
    
    protected class ClockPanel extends JPanel{

        private JLabel clock;
        private JLabel name;
        private final int clockNum;
        
        public ClockPanel(String clockName, final int clockNumber) {
            clockNum = clockNumber;
            name = new JLabel(clockName);
            clock = new JLabel();
        }
        
        public void init(){
            setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
            setBorder(BorderFactory.createCompoundBorder(
                                BorderFactory.createRaisedBevelBorder(), 
                                BorderFactory.createLoweredBevelBorder()));
            this.addMouseListener(new ClockListener());
            this.add(Box.createVerticalStrut(LAYOUT_SPACER));
            this.add(name);
            this.add(clock);
            this.add(Box.createVerticalStrut(LAYOUT_SPACER));
            this.updateClock(clockNum);
        }
        
        public ClockPanel(String clockName, String clockZone){
            clockNum = -1;
            name = new JLabel(clockName);
            clock = new JLabel(clockZone);
        }
        
        protected void initShare(){
            
            setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
            setBorder(BorderFactory.createCompoundBorder(
                                BorderFactory.createRaisedBevelBorder(), 
                                BorderFactory.createLoweredBevelBorder()));
            this.add(Box.createVerticalStrut(LAYOUT_SPACER));
            this.add(name);
            this.add(clock);
            this.add(Box.createVerticalStrut(LAYOUT_SPACER));
        }
        
        protected void updateClock(int clockNumber){
            //String date = MyClockPanel.dateFormatter.format(ca.MANAGER.getClock("101", "1"));
            //TODO chnge to accept Date object once webresource returns it
           String date = ca.MANAGER.getClock(""+userID, ""+clockNumber);
           clock.setText(date);
           this.repaint();
           //this.requestFocus();
            System.out.println("clock #" + clockNumber + " has been updated");
        }
        
        protected void updateClock(String cityZone){
            clock.setText(cityZone);
            this.revalidate();
            this.repaint();
        }

        protected class ClockListener implements MouseListener{

            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.println("Clock #"+ clockNum + " was pressed");
                Object[] options = {
                    "Change Zone",
                    "Remove Clock",
                    "Share Clock",
                    "Clear all clocks",
                    "Cancel"};
                int actionChoice = JOptionPane.showOptionDialog(null,
                    "\tWhat would you like to do?",
                    "Configure Clock",
                    JOptionPane.YES_NO_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    options,
                    options[4]);

                switch(actionChoice){
                    case 0 : {
                        System.out.println("user chose to UPDATE clock"); 
                        //TODO change code to handle the date
                        ArrayList<String> citiesArray = new ArrayList<>();// = {"SYD", "AKL", "DUB", "MEL", "CAI"};
                        
//                        JsonArray array = ca.MANAGER.getAllCities(JsonArray.class);
//                        if(array != null)
//                            for(JsonValue jValue : array){
//                                JsonString jStr = (JsonString)jValue;
//                                citiesArray.add(jStr.getString());
//                            }
//                        Object[] cities = citiesArray.toArray();
                        String cities = ca.MANAGER.getAllCities();
                        StringTokenizer tokeniser = new StringTokenizer(cities, ",");
                        ArrayList<String> cityList = new ArrayList<>();
                        while(tokeniser.hasMoreTokens()){
                            cityList.add(tokeniser.nextToken());
                        }
                        Object[] cityArray = cityList.toArray();
                        String zoneChoice = (String)JOptionPane.showInputDialog(null,
                                            "Select a Time Zone",
                                            "Update " + name.getText(),
                                            JOptionPane.PLAIN_MESSAGE,
                                            null,
                                            cityArray,
                                            cityArray[0]);
                        String date = ca.MANAGER.updateClock(""+userID, ""+clockNum, zoneChoice);
                        myClocks[clockNum].updateClock(clockNum);
                        break;
                    }
                    case 1 : {
                        System.out.println("user chose to REMOVE clock"); 
                        int deleteChoice = JOptionPane.showConfirmDialog(null, "Really? Your clock will be removed"+
                                                            "\nand you won't get it back.",
                                                    "You've Chosen to Clear this Clock", 
                                                    JOptionPane.YES_NO_OPTION,
                                                    JOptionPane.WARNING_MESSAGE);
                        if(deleteChoice == JOptionPane.YES_OPTION){
                            boolean success = ca.MANAGER.removeClockByNumber(boolean.class, ""+userID, ""+ clockNum);
                            System.out.println("clockClear was successful");
                            if(!success){
                                JOptionPane.showMessageDialog(null, 
                                        "I'm sorry but I could not remove the clock.\n"+
                                                "Please call Sez to get this fixed.",
                                        "Clock Removal Failure",
                                        JOptionPane.WARNING_MESSAGE);
                            }
                            else{
                                myClocks[clockNum].updateClock(clockNum);
                            }
                        }
                        break;
                    }
                    case 2 : {
                        //Get observer (obs) details
                        System.out.println("user chose to SHARE clock"); 
                        String obsName = JOptionPane.showInputDialog("Great! You're going to share your clock with someone?" + 
                                                    "\nTell us who:");
                        if(obsName.equals(userName))
                            JOptionPane.showMessageDialog(null, "Haha, nice try!" + "\nSorry but you can't share a clock with yourself!");
                        else{
                            int obsID = USER_NOT_FOUND; 
                            while(obsName != null && obsID == USER_NOT_FOUND){
                                obsID =  ca.MANAGER.getUserID(int.class, obsName);
                                if(obsID != USER_NOT_FOUND)
                                    //Initiate sharing of the clock
                                    //new ClockShareMessageHandler(userID, clockNum, obsID);
                                    msgHandler.sendMessage(""+ 0 + "," + userID + "," + clockNum + "," + obsID);
                                else obsName = JOptionPane.showInputDialog("I'm sorry, I couldn't find that user..." + 
                                                                            "\nDo you want to try again?");
                            }
                        }
                        break;
                    }
                    case 3 : {
                        System.out.println("user chose to CLEAR ALL clocks"); 
                        int deleteAllChoice = JOptionPane.showConfirmDialog(null, "Really? This will remove ALL the clocks"+
                                                            "\nand you won't get them back.",
                                                    "You've Chosen to Clear all Clocks", 
                                                    JOptionPane.YES_NO_OPTION,
                                                    JOptionPane.WARNING_MESSAGE);
                        if(deleteAllChoice == JOptionPane.YES_OPTION){
                            boolean success = ca.MANAGER.removeClocks(boolean.class, ""+userID);
                            System.out.println("clockClear was successful");
                            if(!success){
                                JOptionPane.showMessageDialog(null, 
                                        "I'm sorry but I could not remove all the clocks.\n"+
                                                "Please call Sez to get this fixed.",
                                        "Clock Removal Failure",
                                        JOptionPane.WARNING_MESSAGE);
                            }
                            else{
                                int i = 0;
                                for(ClockPanel cp : myClocks){
                                    cp.updateClock(i++);
                                }
                            }
                        }
                        break;
                    }
                }
            }
// <editor-fold defaultstate="collapsed" desc="Unused MouseListener methods">
/* start of comment
...
*/

            @Override
            public void mousePressed(MouseEvent e) {
                //Not used
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                //Not used
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                //Not used
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //Not used
            }
// </editor-fold>
        }
    }
    
    protected class SharePanel extends JPanel{
        protected JLabel shareLabel;

        public SharePanel() {
            setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
            setBorder(BorderFactory.createTitledBorder("Shared Clocks"));
            shareLabel = new JLabel("No clocks yet :-(");
            add(shareLabel);
        }
        
        public void setShareLabel(String newLabel){
            shareLabel.setText(newLabel);
            this.revalidate();
            this.repaint();
        }
        
        public void addClock(String uname, String zone){
            setShareLabel("");
//            JPanel panel = new JPanel();
//           // panel.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
//            panel.setBorder(BorderFactory.createCompoundBorder(
//                                BorderFactory.createRaisedBevelBorder(), 
//                                BorderFactory.createLoweredBevelBorder()));
//            JLabel name = new JLabel(uname + "'s Clock:");
//            JLabel clock = new JLabel(zone);
//          //  panel.add(Box.createVerticalStrut(LAYOUT_SPACER));
//            panel.add(name);
//            panel.add(clock);
//           // panel.add(Box.createVerticalStrut(LAYOUT_SPACER));
            ClockPanel panel = new ClockPanel(uname, zone);
            panel.initShare();
            this.add(panel);
            this.revalidate();
            this.repaint();
        }
    }
    
    protected static class MenuListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            //TODO show help file.
            // see http://stackoverflow.com/questions/9761727/basic-code-to-display-a-pdf-in-an-existing-jpanel
            //see http://stackoverflow.com/questions/4437910/java-pdf-viewer 
            System.out.println("HELP MENU WAS ACCESSED");
        }
        
    }
    
    public void addSharedClock(String clockOwner, String zone){
        sharePanel.addClock(clockOwner, zone);
    }
    
    public static void main(String[] args) {
        
        String uname = JOptionPane.showInputDialog(null, "Please enter your username:");
        int userID = ca.MANAGER.getUserID(int.class, uname);
        
        if(userID == USER_NOT_FOUND){
            int response = JOptionPane.showConfirmDialog(null,
                    ("Username: '"+uname + "' was not found.\nWould you like to sign up as '"+uname+"?'"), 
                    "Username not found",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
            if(response == JOptionPane.YES_OPTION){
                System.out.println("user opted to sign up");
                userID = ca.MANAGER.signup(int.class, uname);
                System.out.println("new userID is: " + userID);
            }
        }
        
        if(userID != USER_NOT_FOUND){        
            JFrame frame = new JFrame("ClockMe for Desktop");
            MyClockPanel panel = new MyClockPanel(userID, uname);
                panel.init();
            panel.msgThread.start();
            panel.subscribeThread.start();
            JMenuBar menuBar = new JMenuBar();
            JMenu menu = new JMenu("Help");
                menu.getAccessibleContext().setAccessibleDescription(
                                    "How to use ClockMe for Desktop");
                
            JMenuItem menuHelpItem;
                menuHelpItem = new JMenuItem("Open Help");
                menuHelpItem.addActionListener(panel.menuListener);
            menu.add(menuHelpItem);
            menuBar.add(menu);
            frame.setJMenuBar(menuBar);
                
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.getContentPane().add(panel);
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            frame.setSize(WINDOW_WIDTH, screenSize.height);
            frame.setVisible(true);
        }
    }
    
}
