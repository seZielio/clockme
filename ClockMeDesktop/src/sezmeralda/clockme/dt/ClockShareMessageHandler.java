/**
 * Distributed & Mobile Systems: S1 2016 Assignment 4: WebServices and Messaging
 *
 * @author Sez Prouting
 */
package sezmeralda.clockme.dt;

import javax.swing.JOptionPane;
import org.zeromq.ZMQ;

public class ClockShareMessageHandler {
    public static final String TRUE="1";
    
    public ClockShareMessageHandler(){
        
    }
    
    public String sendMessage(String message){
        System.out.println("Message to be send is: " + message);
        System.out.println("ZMQ-RequestClient starting"
         + " with ZeroMQ version " + ZMQ.getVersionString());
        
        // Initiate messaging
        //TODO add timeout for a connection
        ZMQ.Context context = ZMQ.context(1); // thread pool size 1
        ZMQ.Socket socket = context.socket(ZMQ.REQ);
        String response = "";
        try{
            //socket.connect("tcp://sezmeralda.ddns.net:8890"); // production
            socket.connect("tcp://10.0.0.11:8890");       // local LAN testing
            socket.send(message);
            
            byte[] responseArr = socket.recv(0);
            response = new String(responseArr);
            System.out.println("Received back: " + response);
            
//            if(!response.equals(TRUE))
//                JOptionPane.showMessageDialog(null, 
//                        "I'm sorry, the server could not process your request\n maybe you should try again later.", 
//                        "Whoa, something went wrong", 
//                        JOptionPane.ERROR_MESSAGE);
                
        }
        finally{
            socket.close();
            context.term();
            System.out.println("ZMQ-RequestClient has stopped");
        }
        return response;
    }
    
}
