/**
 * Distributed & Mobile Systems: S1 2016
 * Assignment 4: WebServices and Messaging
 *
 * @author Sez Prouting
 */
package sezmeralda.clockme.dt;

import java.util.ArrayList;
import org.zeromq.ZMQ;

public class UpdateSubscriber implements Runnable{

    protected final MyClockPanel panel;
    protected ArrayList<byte[]> topicMap;
    protected ZMQ.Context context;
    protected ZMQ.Socket subscriber;
    
    
    public UpdateSubscriber(MyClockPanel panel) {
        this.panel = panel;
        context = null;
        subscriber = null;
        
        System.out.println("\t\tZMQ-Subscriber starting"
            + " with ZeroMQ version " + ZMQ.getVersionString());
    }
    
    
    @Override
    public void run(){
        
        try{
            
            context = ZMQ.context(1);
            subscriber = context.socket(ZMQ.SUB);
            //subscriber.connect("tcp://sezmeralda.ddns.net:8890");     //Production
            subscriber.connect("tcp://10.0.0.11:8891");                 //Development & Maintenance
            subscriber.subscribe("1".getBytes());
            
            while(!Thread.currentThread().isInterrupted()){
            String address = subscriber.recvStr();
            String contents = subscriber.recvStr();
            
            System.out.println("Address = " + address + " and contents = " + contents);
            }
        }
        finally{
            if(subscriber != null) subscriber.close();
            if(context != null) context.term();
            System.out.println("\t\tZMQ-Subscriber has stopped");
        }
    }
    
    /**
     * Adds a clock & number to the local collection of topics. 
     * @param owner the unique id of the owner of the clock to be added
     * @param clockNum the specific clock which has been shared and which might be updated
     * @return true if the clock is added, false if the combination was already in the collection
     */
    public boolean addClock(int owner, int clockNum){
        byte[] topic = toByteArray(owner, clockNum);
        
        if(!topicMap.contains(topic)){
            topicMap.add(topic);
            return true;
        }
        return false;
    }
    
    /**
     * Removes the ownel:clockNum combination from the collection. Updates will no longer
     * be received for this clock.
     * @param owner The unique ID of the owner of the clock to be removed
     * @param clockNum The specific clock which has been shared and is to be removed
     * @return true if the combination was in the collection and has been removed, false
     * if the combination was not in the collection.
     */
    public boolean removeClock(int owner, int clockNum){
        byte[] topic = toByteArray(owner, clockNum);
        
        if(topicMap.contains(topic)){
            topicMap.remove(topic);
            return true;
        }
        return false;
    }
    
    protected byte[] toByteArray(int owner, int clockNum){
        return ("" + owner + ":" + clockNum).getBytes();
    }
}
