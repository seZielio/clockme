/**
 * Distributed & Mobile Systems: S1 2016
 * Assignment 4: WebServices and Messaging
 *
 * @author Sez Prouting
 */
package sezmeralda.clockme.share;

import java.util.StringTokenizer;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import org.glassfish.jersey.client.ClientProperties;

public class ShareHandler {
    
    public static final int SHARE_CLOCK=0,
                            SHARE_ENQUIRY=1,
                            ACCEPT_SHARE=2,
                            DECLINE_SHARE=3,
                            FAIL=-1;
    public static final String OPERATION_FAIL="0";
                            
    private static final ClockManager CLOCK_MANAGER = new ClockManager();
    
    public static String parseMessage(String shareNotice){
        int messageType, ownerID, observerID, clockNum, shareID;
        System.out.println("RecordKeeper has received: " + shareNotice);
        
        StringTokenizer tokeniser = new StringTokenizer(shareNotice, ",");
        try{
            messageType = Integer.parseInt(tokeniser.nextToken());
            System.out.println("MessageType = " + messageType);
            
            switch(messageType){
                case SHARE_CLOCK : {
                    System.out.println("ClockShare in process..");
                    ownerID = Integer.parseInt(tokeniser.nextToken());
                    clockNum = Integer.parseInt(tokeniser.nextToken());
                    observerID = Integer.parseInt(tokeniser.nextToken());
                    System.out.println("ownerID = " + ownerID + 
                                    "\nclockNum = " + clockNum + 
                                    "\nobserverID = " + observerID );
                    //TODO return clock share ID
                    int success =  CLOCK_MANAGER.setClockShare(int.class, ""+ownerID, ""+clockNum, ""+observerID);
                    if(success != FAIL)
                        return "1";
                }
                case SHARE_ENQUIRY : {
                    System.out.println("Share enquiry in process...");
                    observerID = Integer.parseInt(tokeniser.nextToken());
                    
                    String sharedClock = CLOCK_MANAGER.getPendingShare(""+observerID);
                    System.out.println("Share enquiry will return:" + sharedClock);
                    
                    return sharedClock;
                }
                case ACCEPT_SHARE : {
                    System.out.println("Share acceptance in process...");
                    shareID = Integer.parseInt(tokeniser.nextToken());
                    int acceptanceSuccess = CLOCK_MANAGER.acceptClockShare(int.class, ""+shareID);
                    System.out.println("acceptanceSuccess = " + acceptanceSuccess);
                    
                    return ""+acceptanceSuccess;
                }
                case DECLINE_SHARE : {
                    System.out.println("Share rejection in process...");
                    shareID = Integer.parseInt(tokeniser.nextToken());
                    int declineSuccess = CLOCK_MANAGER.declineClockShare(int.class, ""+shareID);
                    System.out.println("declineSuccess = " + declineSuccess);
                    
                    return ""+declineSuccess;
                }
                default : {return OPERATION_FAIL;}
            }
        }
        catch(Exception e){
            System.err.println("There has been an error parsing the ZMQ message: " + e.toString());
            return OPERATION_FAIL;
        }
    }
/*
http://uni-sezmeralda.pagekite.me/ClockMe/webresources
            client.property(ClientProperties.SUPPRESS_HTTP_COMPLIANCE_VALIDATION, true);
*/

    static class ClockManager {

        private WebTarget webTarget;
        private Client client;
        private static final String BASE_URI = "http://uni-sezmeralda.pagekite.me/ClockMe/webresources";

        public ClockManager() {
            client = javax.ws.rs.client.ClientBuilder.newClient();
            client.property(ClientProperties.SUPPRESS_HTTP_COMPLIANCE_VALIDATION, true);
            webTarget = client.target(BASE_URI).path("myclocks");
        }

        public <T> T removeClockByNumber(Class<T> responseType, String id, String clock) throws ClientErrorException {
            return webTarget.path(java.text.MessageFormat.format("deletebynumber/{0}/{1}", new Object[]{id, clock})).request().delete(responseType);
        }

        public String getPendingShare(String id) throws ClientErrorException {
            WebTarget resource = webTarget;
            resource = resource.path(java.text.MessageFormat.format("checkshare/{0}", new Object[]{id}));
            return resource.request(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(String.class);
        }

        public String infoPage() throws ClientErrorException {
            WebTarget resource = webTarget;
            return resource.request(javax.ws.rs.core.MediaType.TEXT_HTML).get(String.class);
        }

        public String updateClock(String id, String clock, String city) throws ClientErrorException {
            return webTarget.path(java.text.MessageFormat.format("{0}/{1}/{2}", new Object[]{id, clock, city})).request().put(null, String.class);
        }

        public <T> T removeClocks(Class<T> responseType, String id) throws ClientErrorException {
            return webTarget.path(java.text.MessageFormat.format("delete-all/{0}", new Object[]{id})).request().delete(responseType);
        }

        public <T> T signup(Class<T> responseType, String uname) throws ClientErrorException {
            return webTarget.path(java.text.MessageFormat.format("signup/{0}", new Object[]{uname})).request().put(null, responseType);
        }

        public <T> T removeClockByName(Class<T> responseType, String id, String city) throws ClientErrorException {
            return webTarget.path(java.text.MessageFormat.format("deletebyname/{0}/{1}", new Object[]{id, city})).request().delete(responseType);
        }

        public <T> T acceptClockShare(Class<T> responseType, String shareID) throws ClientErrorException {
            return webTarget.path(java.text.MessageFormat.format("accept/{0}", new Object[]{shareID})).request().put(null, responseType);
        }

        public String getUserName(String id) throws ClientErrorException {
            WebTarget resource = webTarget;
            resource = resource.path(java.text.MessageFormat.format("name/{0}", new Object[]{id}));
            return resource.request(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(String.class);
        }

        public String getAllCities() throws ClientErrorException {
            WebTarget resource = webTarget;
            resource = resource.path("citylist");
            return resource.request(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(String.class);
        }

        public String getClock(String id, String clock) throws ClientErrorException {
            WebTarget resource = webTarget;
            resource = resource.path(java.text.MessageFormat.format("{0}/{1}", new Object[]{id, clock}));
            return resource.request(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(String.class);
        }

        public String getAcceptedShares(String observer) throws ClientErrorException {
            WebTarget resource = webTarget;
            resource = resource.path(java.text.MessageFormat.format("accepted-shares/{0}", new Object[]{observer}));
            return resource.request(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(String.class);
        }

        public <T> T declineClockShare(Class<T> responseType, String shareID) throws ClientErrorException {
            return webTarget.path(java.text.MessageFormat.format("decline/{0}", new Object[]{shareID})).request().delete(responseType);
        }

        public <T> T setClockShare(Class<T> responseType, String owner, String clock, String observer) throws ClientErrorException {
            return webTarget.path(java.text.MessageFormat.format("share/{0}/{1}/{2}", new Object[]{owner, clock, observer})).request().put(null, responseType);
        }

        public <T> T getUserID(Class<T> responseType, String uname) throws ClientErrorException {
            WebTarget resource = webTarget;
            resource = resource.path(java.text.MessageFormat.format("{0}", new Object[]{uname}));
            return resource.request(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(responseType);
        }

        public void close() {
            client.close();
        }
    }

    
}
