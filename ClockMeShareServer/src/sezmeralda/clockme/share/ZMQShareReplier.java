/**
 * Distributed & Mobile Systems: S1 2016
 * Assignment 4: WebServices and Messaging
 *
 * @author Sez Prouting
 */
package sezmeralda.clockme.share;

import org.zeromq.ZMQ;

public class ZMQShareReplier implements Runnable{
    public static final String  TRUE="1",
                                FALSE="0";
    
    @Override
    public void run() {
    ZMQ.Context context = null;
    ZMQ.Socket socket = null;
    String request;
    String response;
    
        try{
            System.out.println("ZMQ-ReplyServer starting"
                + " with ZeroMQ version " + ZMQ.getVersionString());
            context = ZMQ.context(1);  // thread pool size 1
            socket = context.socket(ZMQ.REP);
            socket.bind("tcp://*:8890");
            System.out.println("ZMQ-socket bound to 8890");

            while(true){
        /******* WAIT FOR INPUT **********/        
                byte[] bytesReceived = socket.recv(0); // blocking by default
                request = new String(bytesReceived);
                
        /******** PROCESS INPUT **********/        
                response = ShareHandler.parseMessage(request);
                
        /******** SEND RESPONSE *********/
                socket.send(response, 0);  // blocking by default
                System.out.println("Response sent: " + response);
            }
        }
        finally{
            if(socket != null) socket.close();
            if(context != null) context.term();
            System.out.println("ZMQ-ReplyServer has stopped");
        }
    }
}
