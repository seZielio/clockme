/**
 * Distributed & Mobile Systems: S1 2016
 * Assignment 4: WebServices and Messaging
 *
 * @author Sez Prouting
 */
package sezmeralda.clockme.share;

public class Server {
    
    public static void main(String[] args) {
        ZMQUpdatePublisher publishServer = new ZMQUpdatePublisher();
        
        Thread shareReplier = new Thread(new ZMQShareReplier());
        Thread updatePublisher = new Thread(publishServer);
        
        shareReplier.start();
        updatePublisher.start();
        
    }
    
}
