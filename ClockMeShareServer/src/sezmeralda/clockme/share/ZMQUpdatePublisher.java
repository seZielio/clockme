/**
 * Distributed & Mobile Systems: S1 2016
 * Assignment 4: WebServices and Messaging
 *
 * @author Sez Prouting
 */
package sezmeralda.clockme.share;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import org.zeromq.ZMQ;

public class ZMQUpdatePublisher implements Runnable{
    protected ZMQ.Context context;
    protected ZMQ.Socket publisher;
    protected Timer timer;
    protected static int DELAY=10000;

    public ZMQUpdatePublisher() {
        context = null;
        publisher = null;
    }
    
    
    
    @Override
    public void run(){
        ActionListener updateTimer = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                sendUpdates();
            }
        };
        timer = new Timer(DELAY, updateTimer);
        
        System.out.println("ZMQPublisher starting"
            + " with ZeroMQ version " + ZMQ.getVersionString());
        
        context = ZMQ.context(1);
        publisher = context.socket(ZMQ.PUB);
        publisher.bind("tcp://*:8891");
        timer.start();
        
    }
    
    protected void sendUpdates(){
        
        
            /*****   SEND ALL UPDATES  ********/
            
            publisher.sendMore("1");
            publisher.send("This string is destined for subcribers of topic 1 only");
            publisher.sendMore("3");
            publisher.send("This string is destined for subcribers of topic 3 only");
            System.out.println("\n... ... ... Publisher has sent 2 envelopes\n");
    }
    
    public void closeServer(){
        timer.stop();
        if(publisher != null) publisher.close();
        if(context != null) context.term();
        
        System.out.println("Publisher has closed");
    }
}
