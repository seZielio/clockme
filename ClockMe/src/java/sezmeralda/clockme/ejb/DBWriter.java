/**
 * Distributed & Mobile Systems: S1 2016
 * Assignment 4: WebServices and Messaging
 *
 * @author Sez Prouting
 */
package sezmeralda.clockme.ejb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import sezmeralda.clockme.resources.DatabaseBean;

@Stateless
public class DBWriter extends DatabaseBean{
    
    protected Connection connection;
    @EJB
    protected DBProperties clocksDB;   
    @EJB
    protected DBReader readDB;
    protected PreparedStatement getUserID,
                                addNewUser,
                                updateClock0, updateClock1, updateClock2, updateClock3, updateClock4,
                                hasUser,
                                addShare,
                                acceptShare,
                                declineShare;
    
    @PostConstruct
    public void init(){    
        super.init(clocksDB.dbDriver());
        System.out.println("~~~~~~ DBWriter has been constructed");
        
        // These are used for SQL statements to update a specific clock. Between the preamble & suufix
        // the SQL needs to specify the column identifier for the specific clock.
        String updateClockPreamble = "UPDATE " + clocksDB.clocksTable() + " SET ";
        String updateClockSuffix = "=? WHERE " + clocksDB.userID() + "=?";
        
        try{            
            connection = DriverManager.getConnection(clocksDB.dbUrl(), clocksDB.dbUser(), clocksDB.password());
            
            hasUser = connection.prepareStatement("SELECT " + clocksDB.userID() +
                                                   " FROM " + clocksDB.clocksTable() +
                                                  " WHERE " + clocksDB.userID() + "=?");
            
            getUserID = connection.prepareStatement("SELECT " + clocksDB.userID() +
                                                    " FROM "  + clocksDB.clocksTable() +
                                                    " WHERE " + clocksDB.userName() + "=?");
            
            addNewUser = connection.prepareStatement("INSERT INTO " + clocksDB.clocksTable()
                                                              +"( " + clocksDB.userName() + ")"
                                                              +"VALUES(?)" );
            
            updateClock0 = connection.prepareStatement(updateClockPreamble + clocksDB.clock(0) + updateClockSuffix);
            updateClock1 = connection.prepareStatement(updateClockPreamble + clocksDB.clock(1) + updateClockSuffix);
            updateClock2 = connection.prepareStatement(updateClockPreamble + clocksDB.clock(2) + updateClockSuffix);
            updateClock3 = connection.prepareStatement(updateClockPreamble + clocksDB.clock(3) + updateClockSuffix);
            updateClock4 = connection.prepareStatement(updateClockPreamble + clocksDB.clock(4) + updateClockSuffix);
            
            
            addShare = connection.prepareStatement( "INSERT INTO " + clocksDB.sharesTable() +
                                                    " ("    + clocksDB.sharedClockOwner() + ", " 
                                                            + clocksDB.sharedClockNumber() + ", " 
                                                            + clocksDB.sharedClockObserver()+ ", " 
                                                            + clocksDB.isShared() + ")" 
                                               + " VALUES(" + "?, ?, ?, FALSE)");
            
            acceptShare = connection.prepareStatement(  "UPDATE " + clocksDB.sharesTable() +
                                                          " SET " + clocksDB.isShared() + "=TRUE" +
                                                        " WHERE " + clocksDB.shareID() + "=?");
            
            declineShare = connection.prepareStatement( "DELETE FROM " + clocksDB.sharesTable() +
                                                             " WHERE " + clocksDB.shareID() + "=?");
        }
        catch(SQLException e){
            System.err.println("------------>  Error during DBAccessor statement preparation: " + e.getMessage());
            System.out.println("--> connection was " + connection);
        }
    }

    /**
     * Adds a new user to the database, if that user does not already exist
     * @param uname A unique name for the user
     * @return The new user ID for the user if s/he is added. If the username was found
     * to already exist, a negative representation of the user ID is returned. For example if
     * user 'john' with a user ID of '123' is already in the database; -123 will be returned. If
     * the operation fails, DatabaseBean.OPERATION_FAILED is returned.
     */
    public synchronized int addUser(String uname){
        int id = getUserID(uname);
        System.out.println("DB-fetch attempted. id = " + id);
        
        if(id != OPERATION_FAILED){ // user is already in DB. Return -ve value of userID to show it already exists
            id *= -1;
        }
        
        if(id == OPERATION_FAILED){ // user is not already in the DB and needs to be created
            System.out.println("id = -1, ready to insert new user");
            try {
                addNewUser.setString(1, uname);
                addNewUser.executeUpdate();
            } catch (SQLException e) {
                System.err.println(SQL_SYNTAX_ERROR + "'DBWriter.addNewUser': " + e);
                return OPERATION_FAILED;
            }
            catch(Exception e){
                System.err.println(GENERAL_ERROR + "DBWriter.addUser(): " + e);
                return OPERATION_FAILED;
            }
            id = getUserID(uname);
        }
        
        return id;
    }

    /**
     * Provides the user ID for the specified user
     * @param uname The unique user name for which the accompanying ID is requested
     * @return The unique user ID which accompanies uname, or DatabaseBean.OPERATION_FAILED
     * if the user doesn't exist or if there was some other error.
     */
    public synchronized int getUserID(final String uname) {
        ResultSet rs;
        try {
            getUserID.setString(1, uname);
            rs = getUserID.executeQuery();
            if(rs.next())
                return rs.getInt(clocksDB.userID());
            
            rs.close();
            
        } catch (SQLException e) {
            System.err.println(SQL_SYNTAX_ERROR + "'DBWriter.getUserID': " + e.getMessage());
            return OPERATION_FAILED;
        }
        catch(Exception e){
            System.err.println(GENERAL_ERROR + "DBWriter.getUserID(): " + e);
            return OPERATION_FAILED;
        }
              
        return OPERATION_FAILED;
    }
    
    /**
     * Updates a user's clock with the new zone, as specified by the 3-letter city abbreviation
     * @param id The unique user ID of the user who owns the clock to be updated
     * @param clockNum The specific clock which is to be updated - ranges from 0-4
     * @param city The 3-letter city abbreviation to represent the new time zone
     * @return true if the clock was successfully updated, else false
     */
    public synchronized boolean updateClock(final int id, final int clockNum, final String city){
        PreparedStatement updateClock = null;
        
        if(hasUser(id)){
            // uses prepared statemet which updates the column identified by clockNum
            switch(clockNum){
                case 0 : {updateClock = updateClock0; break;}
                case 1 : {updateClock = updateClock1; break;}
                case 2 : {updateClock = updateClock2; break;}
                case 3 : {updateClock = updateClock3; break;}
                case 4 : {updateClock = updateClock4; break;}
            }

            // only clock numbers within the range 0-4 are acceptable.
            // skip DB update if the above switch statement never set updateClock
            if(updateClock != null){
                try {
                    updateClock.setString(1, city);
                    updateClock.setInt(2, id);
                    System.out.println(updateClock);
                    updateClock.executeUpdate();

                    return true;

                } catch (SQLException e) {
                    System.err.println(SQL_SYNTAX_ERROR + "'DBWriter.updateClock': " + e);
                    return false;
                }
                catch(Exception e){
                    System.err.println(GENERAL_ERROR + "DBWriter.getUserID(): " + e);
                    return false;
                }
            }
        }
        return false;
    }
   
    /**
     * Sets all of a user's clocks to null
     * @param id The unique ID of the user who wants to reset his/her clocks
     * @return true if all clocks were successfully set to null, else false,.
     */
    public synchronized boolean resetClocks(int id){
        
        try {
            updateClock0.setString(1, null);
            updateClock0.setInt(2, id);
            updateClock1.setString(1, null);
            updateClock1.setInt(2, id);
            updateClock2.setString(1, null);
            updateClock2.setInt(2, id);
            updateClock3.setString(1, null);
            updateClock3.setInt(2, id);
            updateClock4.setString(1, null);
            updateClock4.setInt(2, id);
            
            updateClock0.executeUpdate();
            updateClock1.executeUpdate();
            updateClock2.executeUpdate();
            updateClock3.executeUpdate();
            updateClock4.executeUpdate();
            
            return true;
        } catch (SQLException e) {
            System.err.println(SQL_SYNTAX_ERROR + "'DBWriter.updateClockX': " + e);
            return false;
        }
        catch(Exception e){
            System.err.println(GENERAL_ERROR + "DBWriter.resetClocks(): " + e);
            return false;
        }
    }
    
    /**
     * Checks to see if a user is already in the DB
     * @param id the unique user ID to be checked
     * @return true if the user is in the DB, else false
     */
    private synchronized boolean hasUser(int id){
        ResultSet rs;
        try {
            hasUser.setInt(1, id);
            rs = hasUser.executeQuery();
            if(rs.next()) return true;
        } catch (SQLException e) {
            System.err.println(SQL_SYNTAX_ERROR + "'DBWriter.hasUser': " + e);
            return false;
        }
        catch(Exception e){
            System.err.println(GENERAL_ERROR + "DBWriter.hasUser(): " + e);
            return false;
        }
        return false;
    }
    
    /**
     * Adds a 'clock share' or 'share' to the database. This consists of an owner/clockNumber/observer
     * combination. This combination should not be duplicated in the DB. The 'shared' flag is initially set
     * to false to indicate the share has been offered but is not yet accepted.
     * @param owner The unique user ID of the owner who has chosen to share the clock
     * @param clock The number of the clock to be shared (0-5) This relates to the owner's clock.
     * @param observer The unique user ID of the user with whom the clock is to be shared. This user
     * will have read-only rights to the clock.
     * @return If the combination is successfully added, he unique share ID (primary key) for the 
     * share combination is returned. Else DatabaseBean.OPERATION_FAILED is returned.
     */
    public synchronized int addShare(int owner, int clock, int observer){
        int shareID;
        System.out.println(">-------> addShare parameters: owner="+owner+", clock="+clock+", observer="+observer);
        shareID = readDB.getShareID(owner, clock, observer);
            
        if(shareID == NOT_FOUND){
            try {
                addShare.setInt(1, owner);
                addShare.setInt(2, clock);
                addShare.setInt(3, observer);
                addShare.executeUpdate();
                shareID =  readDB.getShareID(owner, clock, observer);
            } catch  (SQLException e) {
                System.err.println(SQL_SYNTAX_ERROR + "'DBWriter.addShare': " + e);
                return OPERATION_FAILED;
            }
            catch(Exception e){
                System.err.println(GENERAL_ERROR + "DBWriter.addShare(): " + e);
                return OPERATION_FAILED;
            }
        }
        return shareID;
    }
    
    /**
     * Sets the 'shared' flag on a share combination (see addShare()) to true to indicate the
     * observer does, in fact, wish to observe th clock.
     * @param shareID The unique ID of the share combination which is to be accepted
     * @return Will return DatabaseBean.SUCCESS if the operation was successful; DatabaseBean.NOT_FOUND
     * if the share ID does not exist in the DB; a 0 if the share exists but is already accepted and
     * DatabaseBean.OPERATION_FAILED if there was an exception during execution.
     */
    public synchronized int acceptShare(int shareID){
        System.out.println("~ ~ ~ ~ AcceptShare has been entered");
        ResultSet shares = null;
        int isShared = readDB.isShared(shareID);
        switch(isShared){
            case(-1) : return NOT_FOUND; // share wasn't in the DB
            case(1) : return 0; // share was already accepted.
            case(0) : { // share is still pending acceptance
                System.out.println("share id exists");
                try {
                    acceptShare.setInt(1, shareID);
                    acceptShare.executeUpdate();
                    return SUCCESS; // share accepted

                } catch  (SQLException e) {
                    System.err.println(SQL_SYNTAX_ERROR + "'DBWriter.acceptShare': " + e);
                    return OPERATION_FAILED;
                }
                catch(Exception e){
                    System.err.println(GENERAL_ERROR + "DBWriter.acceptShare(): " + e);
                    return OPERATION_FAILED;
                }
            }
            default : return OPERATION_FAILED;
        }
    }
    
    /**
     * Deletes the share combination from the database.
     * @param shareID The unique ID of the share combination which is to be declined/deleted
     * @return Will return DatabaseBean.SUCCESS if the operation was successful; DatabaseBean.NOT_FOUND
     * if the share ID does not exist in the DB; or DatabaseBean.OPERATION_FAILED if there was 
     * an exception during execution.
     */
    public synchronized int declineShare(int shareID){
        System.out.println("~ ~ ~ ~ DeclineShare has been entered");
        ResultSet shares = null;
        int isShared = readDB.isShared(shareID);
        
        if(isShared > NOT_FOUND){
            try{
                declineShare.setInt(1, shareID);
                declineShare.executeUpdate();
                return SUCCESS;
            } catch  (SQLException e) {
                System.err.println(SQL_SYNTAX_ERROR + "'DBWriter.declineShare': " + e);
                return OPERATION_FAILED;
            }
            catch(Exception e){
                System.err.println(GENERAL_ERROR + "DBWriter.declineShare(): " + e);
                return OPERATION_FAILED;
            }
        }
        return NOT_FOUND;
    }
    
}
