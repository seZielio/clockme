/**
 * Distributed & Mobile Systems: S1 2016
 * Assignment 4: WebServices and Messaging
 *
 * @author Sez Prouting
 */
package sezmeralda.clockme.ejb;

import java.io.IOException;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import sezmeralda.clockme.resources.DatabaseBean;

@Singleton
@Startup
public class DBProperties {
    private Properties properties;
    
    @PostConstruct
    private void init(){
        properties = new Properties();
        try{
            properties.loadFromXML(getClass().getResourceAsStream
            ("../resources/dbAccessConfig.xml"));
        }catch(IOException e){
            System.err.println("======= Cannot access properties file: "+ e.getMessage());
        }
    }

    /*************************************************************************
     *      DATABASE CONNECTION
     * ***********************************************************************/
    /**
     * Provides the required driver for access to the application RDBMS
     * @return String representation of the database driver
     */
    public String dbDriver() {
        return properties.get("dbDriver").toString();
    }
    
    /**
     * Provides the URL of the database which is to be accessed
     * @return String representation of the database URL
     */
    public String dbUrl() {
        return properties.get("dbUrl").toString();
    }

    
    /**
     * Provides the login id for access to the RDBMS
     * @return String representation of the database user account
     */
    public String dbUser() {
        return properties.get("user").toString();
    }

    
    /**
     * Provides the password associated with the user account for access to the RDBMS
     * @return String representation of the access password. See also dbUser() for the associated user account
     */
    public String password() {
        return properties.get("password").toString();
    }

    /*************************************************************************
     *      TABLES
     * ***********************************************************************/
    
    
    /**
     * Provides the name of the DB table which holds the user details and own clocks
     * @return String representation of the table which holds the user details and own clocks
     */
    public String clocksTable() {
        return properties.get("clocks_table").toString();
    }

    
    /**
     * Provides the name of the DB table which holds the details of clocks which have been shared
     * @return String representation of the table which holds the details of clocks which have been shared
     */
    public String sharesTable() {
        return properties.get("shared_clocks_table").toString();
    }

    /************************************************************************
     *     COLUMNS
     **********************************************************************/
    
    
    /**
     * Provides the column name for the user ID number in the table specified by clocksTable()
     * @return String representation of the user ID column in clocksTable()
     */
    public String userID() {
        return properties.get("userID").toString();
    }

    
    /**
     * Provides the column name for the unique username associated with userID() in the DB table specified by clocksTable()
     * @return String representation of the username column in clocksTable()
     */
    public String userName() {
        return properties.get("userName").toString();
    }

    /**
     * Provides the column name for any one of the clocks associated with a user in the DB table specified by clocksTable(). Note that the service provides
     * only 5 (owned) clocks per user.
     * @param clockNumber String representation of the column name for the specified clock
     * @return String representation of the column name for the specified clock
     * @throws ArrayIndexOutOfBoundsException if clockNumber is less than 0 or greater than MAX_CLOCKS
     */
    public String clock(int clockNumber) throws ArrayIndexOutOfBoundsException {
        if(clockNumber >= 0
        && clockNumber < DatabaseBean.MAX_CLOCKS)
            return properties.get("clock"+clockNumber).toString();
        
        else throw new ArrayIndexOutOfBoundsException("In sezmeralda.clockme.ejb.clock() param 'clockNumber' was less than zero or greater than " + DatabaseBean.MAX_CLOCKS);
    }

    /**
     * Provides the column name for the unique ID which defines a specific share combination in sharesTable()
     * @return String representation of the column name for the unique ID which defines a specific share combination in sharesTable()
     */
    public String shareID() {
        return properties.get("shareID").toString();
    }
    
    
    /**
     * Provides the column name for the owner ID of a shared clock in the DB table specified by sharesTable()
     * @return String representation of the column name for the owner which is sharing a clock
     */
    public String sharedClockOwner() {
        return properties.get("ownerID").toString();
    }

    /**
     * Provides the column name for the observer ID of a clock which has been shared as entered into the DB table specified by sharesTable()
     * @return String representation of the column name for an observer of a shared clock
     */
    public String sharedClockObserver() {
        return properties.get("observerID").toString();
    }

    /**
     * Provides the column name of the clock which is being shared, as entered into the DB table specified by sharesTable()
     * @return String representation of the column name for the shared clock
     */
    public String sharedClockNumber() {
        return properties.get("sharedClockNumber").toString();
    }

    /**
     * Provides the column name for the boolean value which specifies a clock has been shared only (false) or shared AND accepted (true) in the DB table 
     * specified by sharesTable()
     * @return  String representation of the column name for the shared status
     */
    public String isShared() {
        return properties.get("isShared").toString();
    }
}
