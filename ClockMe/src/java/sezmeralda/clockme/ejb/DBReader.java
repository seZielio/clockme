/**
 * Distributed & Mobile Systems: S1 2016
 * Assignment 4: WebServices and Messaging
 *
 * @author Sez Prouting
 */
package sezmeralda.clockme.ejb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import sezmeralda.clockme.resources.DatabaseBean;

@Stateless
public class DBReader extends DatabaseBean{
    
    
    protected Connection connection;
    @EJB 
    protected DBProperties clocksDB;
    protected PreparedStatement getClocksByID,
                                getClocksByName,
                                getUserID,
                                getUserName,
                                getShareID,
                                getSharedClockNum,
                                getOwnerName,
                                isShared,
                                getPendingShares,
                                getAcceptedShares;

   @PostConstruct
    public void init(){    
        super.init(clocksDB.dbDriver());
        System.out.println("~~~~~~ DBReader has been constructed");
        
        
        try{            
            connection = DriverManager.getConnection(clocksDB.dbUrl(), clocksDB.dbUser(), clocksDB.password());
            
            getClocksByID = connection.prepareStatement("SELECT " + clocksDB.clock(0) + ", "
                                                              + clocksDB.clock(1) + ", "
                                                              + clocksDB.clock(2) + ", "
                                                              + clocksDB.clock(3) + ", "
                                                              + clocksDB.clock(4)
                                                   + " FROM " + clocksDB.clocksTable()
                                                  + " WHERE " + clocksDB.userID() + "=?");
            
            getClocksByName = connection.prepareStatement("SELECT " + clocksDB.clock(0) + ", "
                                                              + clocksDB.clock(1) + ", "
                                                              + clocksDB.clock(2) + ", "
                                                              + clocksDB.clock(3) + ", "
                                                              + clocksDB.clock(4)
                                                   + " FROM " + clocksDB.clocksTable()
                                                  + " WHERE " + clocksDB.userName() + "=?");            
            
            getUserID = connection.prepareStatement("SELECT " + clocksDB.userID() +
                                                     " FROM " + clocksDB.clocksTable() +
                                                    " WHERE " + clocksDB.userName() + "=?");
            
            getUserName = connection.prepareStatement("SELECT " + clocksDB.userName() +
                                                       " FROM " + clocksDB.clocksTable() +
                                                      " WHERE " + clocksDB.userID() + "=?");
            
            getShareID = connection.prepareStatement("SELECT " + clocksDB.shareID() +
                                                      " FROM " + clocksDB.sharesTable() + 
                                                     " WHERE " + clocksDB.sharedClockOwner() + "=?" +
                                                       " AND " + clocksDB.sharedClockNumber() + "=?" +
                                                       " AND " + clocksDB.sharedClockObserver() + "=?");
            
            getSharedClockNum = connection.prepareStatement("SELECT " + clocksDB.sharedClockNumber() + 
                                                            " FROM " + clocksDB.sharesTable() + 
                                                           " WHERE " + clocksDB.shareID() + "=?");
            
            getOwnerName = connection.prepareStatement("SELECT " + clocksDB.userName() +
                                                        " FROM " + clocksDB.clocksTable() +
                                                       " WHERE " + clocksDB.userID() + "=" + 
                                                                "(SELECT " + clocksDB.sharedClockOwner() +
                                                                " FROM " + clocksDB.sharesTable() +
                                                                " WHERE " + clocksDB.shareID() + "=?)");
            
            isShared = connection.prepareStatement("SELECT " + clocksDB.isShared() +
                                                    " FROM " + clocksDB.sharesTable() +
                                                    " WHERE " + clocksDB.shareID() + "=?");
            
            getPendingShares = connection.prepareStatement( "SELECT "   + clocksDB.shareID() +", "
                                                                        + clocksDB.sharedClockOwner() +", "
                                                                        + clocksDB.sharedClockNumber() +
                                                               " FROM " + clocksDB.sharesTable() +
                                                              " WHERE " + clocksDB.sharedClockObserver() + "=?" +
                                                                " AND " + clocksDB.isShared() + "=FALSE");
            
            getAcceptedShares = connection.prepareStatement("SELECT " + clocksDB.shareID() + 
                                                             " FROM " + clocksDB.sharesTable() + 
                                                            " WHERE " + clocksDB.sharedClockObserver() + "=?" +
                                                              " AND " + clocksDB.isShared() + "=TRUE");
        }
        catch(SQLException e){
            System.err.println("------------>  Error during DBAccessor statement preparation: " + e.getMessage());
            System.out.println("--> connection was " + connection);
        }
    }
    
    /**
     * Provides the unique user ID which is associated with a given user name
     * @param uname The unique String identifier of the owner of a set of clocks
     * @return int user ID which is associated with the specified username.
     */
    public synchronized int getUserID(String uname){
        int id = NOT_FOUND;
        ResultSet results;
        
        try {
            getUserID.setString(1, uname);
            results = getUserID.executeQuery();
            if(results.next()){
                id = results.getInt(clocksDB.userID());
            }
            results.close();
            
        } catch (SQLException e) {
                System.err.println(SQL_SYNTAX_ERROR + "'DBReader.getUserID': " + e);
                return OPERATION_FAILED;
            }
            catch(Exception e){
                System.err.println(GENERAL_ERROR + "DBReader.getUserID(): " + e);
                return OPERATION_FAILED;
            }
        
        return id;
    }
    
    /**
     * Allows access to the user name which accompanies a given user ID
     * @param id The unique user ID which will identify the user name
     * @return The unique user name which accompanies the user ID, else
     * DatabaseBean.ERROR if the id is not in the database or if there is an
     * error during execution.
     */
    public synchronized String getUserName(int id){
        ResultSet results;
        String uname = ERROR;
        try {
            getUserName.setInt(1, id);
            results = getUserName.executeQuery();
            
            if(results.next())
                uname = results.getString(clocksDB.userName());
            
        } catch(SQLException e) {
            System.err.println(SQL_SYNTAX_ERROR + "'DBReader.getUserName': " + e);
            return ERROR;
        }
        catch(Exception e){
            System.err.println(GENERAL_ERROR + "DBReader.getUserName(): " + e);
            return ERROR;
        }
        return uname;
    }
    
    //public Date getClock(int id, int clock){
    //TODO change return type back to Date
    /**
     * Provides the 3-letter city abbreviation which represents the zone which
     * the subject clock is set to.
     * @param id The unique user ID of the owner of the clock
     * @param clock The specific clock which is being requested (in the range 0-4)
     * @return the 3-letter city abbreviation which represents the zone which
     * the subject clock is set to.
     */
    public synchronized String getClock(int id, int clock){
        ResultSet allClocks;
        String city = ERROR;
        try {
            getClocksByID.setInt(1, id);
            allClocks = getClocksByID.executeQuery();
            if(allClocks.next()){
                city = allClocks.getString(clocksDB.clock(clock));
            }
            allClocks.close();
        } catch (SQLException e) {
            System.err.println(SQL_SYNTAX_ERROR + "'DBReader.getClocksByID': " + e);
            return ERROR;
        }
        catch(Exception e){
            System.err.println(GENERAL_ERROR + "DBReader.getClock(): " + e);
            return ERROR;
        }
        
        return city;
    }
    
    /**
     * This method takes a zone (as specified by a three-letter city abbreviation) and
     * finds the first instance in an owner's list. The clock number is determined from
     * the database column identifier.
     * @param id The unique user ID of the clock owner
     * @param clockZone A three-letter city abbreviation of the clock zone who's number is unknown.
     * @return The specific clock (0-5) which is the first clock to contain the given zone
     */
    public synchronized int getClockNum(int id, String clockZone){
        int clockNum = NOT_FOUND;
        String currentZone = "";
        ResultSet rs;
        int i = 0;
        
        try {
            getClocksByID.setInt(1, id);
            rs = getClocksByID.executeQuery();
            
            /* Three cases to break the following while loop:
                a) currentZone == clockZone
                b) i >= MAX_CLOCKS
                c) both a & b  <= this happes when clockZone is the LAST clock
              If the clockZone is found, the while loop breaks with a) always being true
              therefore if currentZone == clockZone, the clock number will be i-1 (i was 
              incremented before the loop breaks. */
                
            if(rs.next()){
              String tempZone;
              while(!currentZone.equals(clockZone)
                      && i < MAX_CLOCKS){
                  if((tempZone=rs.getString(clocksDB.clock(i))) != null)
                    currentZone = tempZone; //(i++));
                  System.out.println("#-----#  Getting clock number, " + i++ + ": " + currentZone);
              }  
            }
            rs.close();
            
            if(currentZone.equals(clockZone))
                clockNum = i-1;
            System.out.println("final clockNum = " + clockNum);
            
        } catch (SQLException e) {
            System.err.println(SQL_SYNTAX_ERROR + "'DBReader.getClocks': " + e);
            return OPERATION_FAILED;
        }
        catch(Exception e){
            System.err.println(GENERAL_ERROR + "DBReader.getClockNum(): " + e);
            return OPERATION_FAILED;
        }
        return clockNum;
    }
    
    /**
     * Returns a CSV String detailing the owner and clock number of a pending clock share. Only
     *  the first share assigned to the observer is returned, this method must be called again 
     *  to discover other pending shares.
     * @param observer The unique id of the user who is permitted to observe the shared clock
     * @return A CSV string in the following pattern shareFound:share_id:owner:clockNumber where shareFound = 1
     * if there was a pending share found for this observer, or 0 if no shares were found; share_id is the DB
     * identifier for the specific share; owner is the unique userID of the owner who shared the clock 
     * and clockNumber specifies which of the owner's clocks is being shared.
     */
    public synchronized String getPendingShare(int observer){
        ResultSet shareDetails = null;
        String result = NOT_FOUND_STRING;
        try {
            getPendingShares.setInt(1, observer);
            shareDetails = getPendingShares.executeQuery();
            if(shareDetails.next()){
                result = FOUND_STRING + "," 
                        + shareDetails.getInt(clocksDB.shareID()) + ","
                        + shareDetails.getInt(clocksDB.sharedClockOwner()) + ","
                        + shareDetails.getInt(clocksDB.sharedClockNumber());
            }
            shareDetails.close();
        } catch (SQLException e) {
            System.err.println(SQL_SYNTAX_ERROR + "'DBReader.getPendingShares': " + e);
            return ERROR;
        }
        catch(Exception e){
            System.err.println(GENERAL_ERROR + "DBReader.getPendingShare(): " + e);
            return ERROR;
        }
        return result;
    }
    
    /**
     * Provides all the shares which a user has accepted and is observing
     * @param observerID the unique user ID of the user who is observing one
     * or more clocks
     * @return An ArrayList with the shareID values of all clocks which are being
     * observed by observerID. If there are no clocks being observed, an empty ArrayList
     * is returned.
     */
    public synchronized ArrayList<Integer> getAcceptedShares(int observerID){
        ResultSet shareResults;
        ArrayList<Integer> shares = new ArrayList<>();
                
        try {
            getAcceptedShares.setInt(1, observerID);
            shareResults = getAcceptedShares.executeQuery();
            
            while(shareResults.next())
                shares.add(shareResults.getInt(clocksDB.shareID()));
            shareResults.close();
        } catch (SQLException e) {
            System.err.println(SQL_SYNTAX_ERROR + "'DBReader.getAcceptedShares': " + e);
            return null;
        }
        catch(Exception e){
            System.err.println(GENERAL_ERROR + "DBReader.getAcceptedShares(): " + e);
            return null;
        }
        return shares;
    }
    
    /**
     * Checks to see if a specific share combination is (still) in the DB
     * @param shareID The unique share ID of the combination
     * @return -1 if the share is not in the DB, 0 if the share exists but is still
     * pending (has not been accepted by the observer) and 1 if the share exists and
     * has been accepted.
     */
    public synchronized int isShared(int shareID){
        ResultSet shareResults;
        boolean shared = false; // does/does not exist in the DB
        boolean accepted = false; // has/has not been accepted
        
        try {
            isShared.setInt(1, shareID);
            shareResults = isShared.executeQuery();
            
            if(shareResults.next()){
                shared = true; // there was a result, the share exists
                accepted = shareResults.getBoolean(clocksDB.isShared());
                shareResults.close();
            }
        } catch (SQLException e) {
            System.err.println(SQL_SYNTAX_ERROR + "'DBReader.isShared': " + e);
            return OPERATION_FAILED;
        }
        catch(Exception e){
            System.err.println(GENERAL_ERROR + "DBReader.isShared(): " + e);
            return OPERATION_FAILED;
        }
        System.out.println(" ~ ~ ~ ~ shared = " + shared);
        if(!shared) return -1; // is not in DB
            else if(accepted) return 1; // is in DB and has already been accepted
                else return 0; // is in DB and is still pending acceptance
    }
    
    /**
     * Accesses the unique share ID of a specific share combination, if it exists
     * @param owner The unique user ID of the clock's owner
     * @param clock The specific clock being shared
     * @param observer The unique ID of the user who is observing (read-only access) the clock
     * @return The unique share ID of a specific share combination, or else 
     * DatabaseBean.OPERATION_FAILED if there was an exception during the operation.
     */
    public synchronized int getShareID(int owner, int clock, int observer){
        ResultSet result;
        int shareID = NOT_FOUND;
        try {
            getShareID.setInt(1, owner);
            getShareID.setInt(2, clock);
            getShareID.setInt(3, observer);
            
            result = getShareID.executeQuery();
            if(result.next())
                shareID = result.getInt(clocksDB.shareID());
            result.close();
        } catch (SQLException e) {
            System.err.println(SQL_SYNTAX_ERROR + "'DBReader.getPendingShares': " + e);
            return OPERATION_FAILED;
        }
        catch(Exception e){
            System.err.println(GENERAL_ERROR + "DBReader.getPendingShare(): " + e);
            return OPERATION_FAILED;
        }
        return shareID;
    }
    
    /**
     * Provides a string combination of owner and zone in the format of "owner,zone"
     * @param shareID The unique share ID for the specific share combination being sought.
     * @return If the share exists in the DB this method will return a String 
     * containing the owner ID and the city zone in the following format:
     * "owner,zone". If the share is not found in the DB, the method returns 
     * DatabaseBean.NOT_FOUND_STRING, and if there is an exception during execution
     * DatabeseBean.ERROR is returned.
     */
    public synchronized String getSharePair(int shareID){
        ResultSet results;
        String owner;
        int clockNum;
        String zone;
        String sharePair = NOT_FOUND_STRING;
        
        try {
            // GET OWNER NAME
            getOwnerName.setInt(1, shareID);
            results = getOwnerName.executeQuery();
            if(results.next()){
                owner = results.getString(clocksDB.userName());
                System.out.println("OWNER = " + owner);

                // GET CLOCK NUMBER
                getSharedClockNum.setInt(1, shareID);
                results = getSharedClockNum.executeQuery();
                results.next();
                clockNum = results.getInt(clocksDB.sharedClockNumber());
                System.out.println("CLOCK_NUM = " + clockNum);
                
                // GET CLOCK ZONE
                getClocksByName.setString(1, owner);
                results = getClocksByName.executeQuery();
                results.next();
                zone = results.getString(clocksDB.clock(clockNum));
                System.out.println("ZONE = " + zone);
                
                sharePair = owner + "," + zone;
                results.close();
            }
        } catch (SQLException e) {
            System.err.println(SQL_SYNTAX_ERROR + "in DBReader.getSharePair(): " + e);
            return ERROR;
        }
        catch(Exception e){
            System.err.println(GENERAL_ERROR + "DBReader.getSharePair(): " + e);
            return ERROR;
        }
        return sharePair;
    }
}
