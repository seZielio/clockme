/**
 * Distributed & Mobile Systems: S1 2016
 * Assignment 4: WebServices and Messaging
 *
 * @author Sez Prouting
 */
package sezmeralda.clockme.resources;

public abstract class DatabaseBean {
    
    public static final String  SQL_SYNTAX_ERROR="~~~~~~~~~~~ Error in SQL syntax for prepared statement ",
                                GENERAL_ERROR="Unknown error in ",
                                ERROR="Operation was unsuccessful, please contact your service administrator",
                                NOT_FOUND_STRING="0",
                                FOUND_STRING="1";
    public static final int     OPERATION_FAILED=-1,
                                SUCCESS=1,
                                NOT_FOUND=-1,  //Before you change this value, some children check that results are > NOT_FOUND!!
                                MAX_CLOCKS=5;
    
    /**
     * Setup for database connection
     * @param forName String providing the database driver which will be used by child classes
     * to access the database
     */
    protected void init(String forName){
        System.out.println("~~~~~~ DBReader has been constructed");
        
        try{
            Class.forName(forName);
        }
        catch(ClassNotFoundException e){
            System.err.println("---------------> DB Driver class not found: " + e.getMessage());
        }
    }        
}
