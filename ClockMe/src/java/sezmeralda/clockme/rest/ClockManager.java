/**
 * Distributed & Mobile Systems: S1 2016
 * Assignment 4: WebServices and Messaging
 *
 * @author Sez Prouting
 */
package sezmeralda.clockme.rest;

import java.util.ArrayList;
import java.util.Date;
import javax.annotation.ManagedBean;
import javax.ejb.EJB;
//import javax.json.Json;
//import javax.json.JsonArray;
//import javax.json.JsonObject;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST; //POST should be used for updates. Change this when you have the time
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import sezmeralda.clockme.ejb.DBReader;
import sezmeralda.clockme.ejb.DBWriter;
import sezmeralda.clockme.resources.DatabaseBean;

@ManagedBean
@Path("myclocks")
public class ClockManager {

    @EJB
    protected DBReader readFromDB;
    @EJB
    protected DBWriter writeToDB;
    public static final int NO_SUCCESS=-1, NOT_FOUND=-1;

    /**
     * Creates a new instance of ClockManager
     */
    public ClockManager() {
    }

/***********************************************************************************
 *      GET = READ 
 ***********************************************************************************/    
    
    /**
     * Retrieves instructions on how to use ClockMe
     * @return HTML page which describes the resource
     */
    @GET
    //@Produces(MediaType.APPLICATION_JSON)
    @Produces("text/html")
    public String infoPage() {
        //TODO provide toc & instructions
        return "<HTML><H3>You have reached the ClockMe RESTFul web resource.</H3></HTML>";
    }
    
    
    /**
     * When passed a unique user name, this method provides the accompanying unique userID.
     * @param uname A unique user name.
     * @return The int userID for the provided user name, or else DatabaseBean.NOT_FOUND if the user does not exist or there is some error.
     */
    @GET
    //@Produces(MediaType.APPLICATION_JSON)
    @Produces("text/plain")
    @Path("{uname}")
    public int getUserID(@PathParam("uname")String uname){
        
        return readFromDB.getUserID(uname);
    }
    
    /**
     * When passed a unique user id, this method provides the accompanying unique user name.
     * @param id a unique user id
     * @return The String user name for the provided user ID, else DatabaseBean.ERROR 
     * if the id is not in the database or if there is an error during execution.
     */
    @GET
    //@Produces(MediaType.APPLICATION_JSON)
    @Produces("text/plain")
    @Path("/name/{id}")
    public String getUserName(@PathParam("id") int id){
        
        return readFromDB.getUserName(id);
    }
    
    
    /**
     * Provides the date of an owner's specific clock, given the owner's userID and the clock number.
     * @param id The clock owner's unique user ID
     * @param clock The specific clock owned by userID. Currently ranges from 0 - 4
     * @return A String representing the three letter city zone of the requested clock
     */
    @GET
    //@Produces(MediaType.APPLICATION_JSON)
    @Produces("text/plain")
    @Path("{id}/{clock}")
    //public Date getClock(@PathParam("id") int id, @PathParam("clock") int clock){
    public String getClock(@PathParam("id") int id, @PathParam("clock") int clock){
        //TODO change return type back to Date
        return readFromDB.getClock(id, clock);
        
    }
    
    /**
     * Provides a list of all the cities which can be used to set the clock
     * time zones
     * @return A list of all the cities which can be used to set clock time zones.
     */
    @GET
    //@Produces(MediaType.APPLICATION_JSON)
    @Produces("text/plain")
    @Path("citylist")
    public String getAllCities(){
//        JsonArray cityArray = Json.createArrayBuilder()
//                .add("SYD")
//                .add("MEL")
//                .add("DUB")
//                .add("LDN")
//                .add("AKL")
//                .build();

//                <RANT>
// <editor-fold defaultstate="collapsed">
/*
    NEITHER JAXB NOR JSON REPRESENTATIONS OF List<String> OR JsonArray COULD BE SENT
    BY THIS ABOMINAL SYSTEM. 
    CONSISTENTLY I WAS HIT WITH THE LIKES OF: 
        Severe:   MessageBodyWriter not found for media type=text/xml, type=class java.util.Arrays$ArrayList, genericType=java.util.List<java.lang.String>.
    ON THE SERVER SIDE;

    OR SOMETHING SIMILAR TO 
        MessageBodyWriter not found for media type=text/JSON, type=class javax.json.JsonArray
    ON THE CLIENT SIDE. 

    I'VE SPENT TOO MANY HOURS F*#KING AROUND ON THE NET TO FIND A SOLUTION AND I HAVE RUN
    OUT OF TIME. IF YOU'RE READING THIS PLEASE CONSIDER ADDING SOME USEFUL JAXB OR JSON INSTRUCTIONS TO THE 
    MANUAL FOR THIS PAPER.  

    (Just because those spoilt BCIS kids learn it doesn't mean us poor cousins in the BEngTech will magically know it too.)
*/
// </editor-fold>
//                </RANT>

        return "SYD,LDN,AKL,CAI,SFO,MEL,DUB";
    }
    
    
    /**
     * Returns a CSV String detailing the owner and clock number of a pending clock share. Only
     *  the first share assigned to the observer is returned, this method must be called again 
     *  to discover other pending shares.
     * @param id The unique id of the user who is permitted to observe the shared clock
     * @return A CSV string in the following pattern shareFound:share_id:owner:clockNumber where shareFound = 1
     * if there was a pending share found for this observer, or 0 if no shares were found; share_id is the DB
     * identifier for the specific share; owner is the unique userID of the owner who shared the clock 
     * and clockNumber specifies which of the owner's clocks is being shared.
     */
    @GET
    //@Produces(MediaType.APPLICATION_JSON)
    @Produces("text/plain")
    @Path("checkshare/{id}")
    //public Date getClock(@PathParam("id") int id, @PathParam("clock") int clock){
    public String getPendingShare(@PathParam("id") int id){
        //TODO change return type back to Date
        return readFromDB.getPendingShare(id);
    }
    
    /**
     * Provides ownerID and clock number for all accepted shares which a user is observing.
     * @param observer The unique user ID of the user who is observing a shared clock
     * @return A string representing the ownerId and clock number for each share. Provided in
     * the form owner,clockNum;owner,clockNum;owner,clockNum
     */
    @GET
    //@Produces(MediaType.APPLICATION_JSON)
    @Produces("text/plain")
    @Path("accepted-shares/{observer}")
    public String getAcceptedShares(@PathParam("observer") int observer){
        String result = "";
        ArrayList<Integer> shareArray = readFromDB.getAcceptedShares(observer);
        
        
        for(Integer shareID : shareArray){
            String temp = readFromDB.getSharePair((int)shareID);
            result += (temp + ";");
        }
        System.out.println("RESULT = " + result);
        return result;
    }
    
/***********************************************************************************
 *      PUT = CREATE 
 ***********************************************************************************/    
    
    /**
     * Allows a new user to sign up. Successful user additions will return a unique userID. NO_SUCCESS is returned 
     * if the user is not successfully added.
     * @param uname A String representation of a user name. If the name is already in the database NO_SUCCESS will be returned
     * @return A positive integer which is the new user's ID number or NO_SUCCESS if the new user was not successfully added to the DB.
     */
    @PUT
    //@Produces(MediaType.APPLICATION_JSON)
    @Produces("text/plain")
    @Path("signup/{uname}")
    public int signup(@PathParam("uname") String uname){
        int dbCheck = getUserID(uname);                
        
        if(dbCheck == DatabaseBean.NOT_FOUND){
            return writeToDB.addUser(uname);
        }
        
        return NO_SUCCESS;
    }
    
    
    /**
     * Adds the owner, clock number and observer to the database as an offered share
     * @param owner The unique ID of the owner who wishes to share the clock
     * @param clock The specific clock which is to be shared. Should be in the range
     * of 0 - 4 inclusive
     * @param observer The unique ID of the user who is invited to observe the clock
     * @return If the combination is successfully added, he unique share ID for the 
     * share combination is returned. Else DatabaseBean.OPERATION_FAILED is returned.
     */
    @PUT
    //@Produces(MediaType.APPLICATION_JSON)
    @Produces("text/plain")
    @Path("share/{owner}/{clock}/{observer}")
    public int setClockShare(@PathParam("owner") int owner,
                            @PathParam("clock") int clock, 
                            @PathParam("observer") int observer){
        
        System.out.println("~~~~~~ setClockShare successfully entered\n" +
                "ownerID = " + owner + 
                          "\nclockNum = " + clock + 
                          "\nobserverID = " + observer );
        return writeToDB.addShare(owner, clock, observer);
    }
    
 
    
/***********************************************************************************
 *      DELETE = DELETE 
 ***********************************************************************************/    
    
    /**
     * Resets a clock to null, as specified by the clock number (from 0 to 4).
     * @param id the int ID of the owner of the clock.
     * @param clock the int number of the clock to be removed.
     * @return true if the clock was successfully set to null, else false.
     */
    @DELETE
    //@Produces(MediaType.APPLICATION_JSON)
    @Produces("text/plain")
    @Path("deletebynumber/{id}/{clock}")
    public boolean removeClockByNumber(@PathParam("id") int id,
                                        @PathParam("clock") int clock){
        System.out.println("~~~~~~~~~~~~~~ removeClockByNumber id = " + id + ", clockNum = " + clock);
        
        return writeToDB.updateClock(id, clock, null);
    }
    
    /**
     * Sets the first instance of a clock in the specified time zone to null. Any other clock
     * with the same time zone remains unchanged.
     * @param id The user ID of the clock owner.
     * @param city The time zone (by three-letter city abbreviation) of the clock to be removed
     * @return True if the clock was set to null. False if the clock was not set to null or was
     * not found in the owner's database.
     */
    @DELETE
    //@Produces(MediaType.APPLICATION_JSON)
    @Produces("text/plain")
    @Path("deletebyname/{id}/{city}")
    public boolean removeClockByName(@PathParam("id") int id,
                                     @PathParam("city") String city){
        System.out.println("~~~~~~~~~~~~~~ removeClockByName id = " + id + ", city = " + city);
        
        int clockNumber;
        if((clockNumber = readFromDB.getClockNum(id, city)) != DatabaseBean.NOT_FOUND){
            return removeClockByNumber(id, clockNumber);
        }
        
        return false;
    }
    
    /**
     * Resets all clocks owned by one user to null.
     * @param id The user ID of the owner whose clocks will be reset.
     * @return True if the clocks were reset, else false.
     */
    @DELETE
    //@Produces(MediaType.APPLICATION_JSON)
    @Produces("text/plain")
    @Path("delete-all/{id}")
    public boolean removeClocks(@PathParam("id") int id){
        System.out.println("~~~~~~~~~~~~~~ removeClocks id = " + id);
        
        return writeToDB.resetClocks(id);
    }

    
    /**
     * The opposite response to acceptClockShare, this method deletes the owner/clock/observer
     * set from the database.
     * @param shareID  The unique ID which identifies an owner/clock/observer combination,
     * see getPendingShare or DBReader's getShareId to help identify this value
     * @return DatabaseBean.SUCCESS (1) if the share was successfully declined (deleted), 
     * DatabaseBean.NOT_FOUND (-1) if the shareID was not found or if there was an error
     */
    @DELETE
    //@Produces(MediaType.APPLICATION_JSON)
    @Produces("text/plain")
    @Path("decline/{shareID}")
    public int declineClockShare(@PathParam("shareID") int shareID){
        
        return writeToDB.declineShare(shareID);
    }
    
    
/***********************************************************************************
 *      POST = UPDATE   ----> haven't got this working just yet
 *                      ----> still set to PUT
 ***********************************************************************************/    
    
    /**
     * Allows a user to change the time zone of an existing clock.
     * @param id An int for the ID of the clock owner.
     * @param clock An int in the range: 0-4 to indicate which of the user's five clocks is to be changed.
     * @param city A String containing the three-letter city abbreviation for the new (updated) time zone.
     * @return A Date object with the current DTG of the new city.
     */
    @PUT //TODO change to post
    //@Consumes("text/plain")
    //@Produces(MediaType.APPLICATION_JSON)
    @Produces("text/plain")
    @Path("{id}/{clock}/{city}")
    //public Date updateClock(@PathParam("id") int id,
    public String updateClock(@PathParam("id") int id,
                            @PathParam("clock") int clock, 
                            @PathParam("city") String city){
        System.out.println("PUT updateClock: id=" + id + " and clock=" + clock);
        
        //TODO get actual date from T&D.com
        //TODO set return type back to Date
        
        boolean writeSuccessful = writeToDB.updateClock(id, clock, city);
        System.out.println("ClockManager.updateClock() success status is: " + writeSuccessful);
        
        if(writeSuccessful)
            return new Date().toString();
        else return new Date(1).toString();
    }
    
     /**
     * Provides the means by which a user can accept a clock which has been offered
     * up as a share
     * @param shareID The unique ID which identifies an owner/clock/observer combination,
     * see getPendingShare or DBReader's getShareId to help identify this value
     * @return DatabaseBean.SUCCESS (1) if the share was successfully accepted, 
     * DatabaseBean.NOT_FOUND (-1) if the shareID was not found or if there was an error
     * and 0 if the shareID was found but the share has already been accepted.
     */
    @PUT // TODO change to post
    @Produces("text/plain")
    @Path("accept/{shareID}")
    public int acceptClockShare(@PathParam("shareID") int shareID){
        System.out.println("~ ~ ~ shareId as received by ClockManager = " + shareID);
        return writeToDB.acceptShare(shareID);
    }
}
